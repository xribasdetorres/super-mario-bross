key_right = keyboard_check(vk_right);
key_left = -keyboard_check(vk_left);
key_jump = keyboard_check(vk_space);

move = key_left + key_right;
hsp = move * movespeed;


if (vsp < 10)
{
    vsp += grav;
}

if (place_meeting(x, y+vsp, obj_SueloN1) or place_meeting(x, y+vsp, obj_Pared))
{
    if(grounded != 0 && !key_jump)
    {
        hkp_count = 0;
        jumping = false
    }
    else
    {
        jumping = true;
    }
    
    grounded = 0;
    vsp = key_jump * -jumpspeed;
}

if(grounded == 0)
{
    hsp_jump_applied = 0;
}

if(move != 0 && grounded != 0)
{
    hkp_count++;
}
else if(move==0 && grounded != 0)
{
    hkp_count = 0;
}

if(jumping)
{
    if(hsp_jump_applied == 0)
    {
        hsp_jump_applied = sign(move)
    }
    
    if(hkp_count >= hkp_count_small)
    {
        hsp = 0;
    }
    else if(hkp_count >= hkp_count_small && hkp_count <= hkp_count_big)
    {
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }
    else
    {
        hsp = hsp_jump_applied * hsp_jump_constant_big;
    }
}

//colision horizontal
if(place_meeting(x+hsp, y, obj_Pared) or place_meeting(x+hsp, y, obj_muro1))
{
   while(!place_meeting(x+sign(hsp), y, obj_Pared) or !place_meeting(x+hsp, y, obj_muro1))
   {
       x += sign(hsp);
   }
   hsp = 0;
}
x += hsp;

//colision vertical
if(place_meeting(x, y+vsp, obj_SueloN1) or place_meeting(x, y+vsp, obj_Pared) )
{
   while(!place_meeting(x, y+vsp, obj_SueloN1) or !place_meeting(x, y+sign(vsp), obj_Pared))
   {
       y += sign(vsp);
   }
   vsp = 0;
}
y += vsp;

if(invisible == true)
{
    if(vsp == 0)
    {
        if(hsp == 0)
        {
            sprite_index = spr_personaje_idle_inv;
        }else{
            if ((sprite_index != spr_Personaje_walk_left_inv) and (sprite_index != spr_Personaje_Walk_inv))
            {
                image_index = 0;
            }
            if(hsp > 0)
            {
                sprite_index = spr_Personaje_Walk_inv;
            }
            else
            {
                sprite_index = spr_Personaje_walk_left_inv;
            }
        }   
           
    }
    else
    {
        if (vsp < 0)
        {
            sprite_index = spr_Personaje_jump_inv;
        }
        else
        {
            sprite_index = spr_Personaje_falling_inv;
        }
    }
}
else
{
    if(vsp == 0)
    {
        if(hsp == 0)
        {
            sprite_index = spr_Personaje_Idle;
        }else{
            if ((sprite_index != spr_Personaje_walk_left) and (sprite_index != spr_Personaje_Walk))
            {
                image_index = 0;
            }
            if(hsp > 0)
            {
                sprite_index = spr_Personaje_Walk;
            }
            else
            {
                sprite_index = spr_Personaje_walk_left;
            }
        }   
           
    }
    else
    {
        if (vsp < 0)
        {
            sprite_index = spr_Personaje_jump;
        }
        else
        {
            sprite_index = spr_Personaje_falling;
        }
    }
}


if(obj_Personaje.move < 0 or obj_Personaje.move > 0)
{
    empezar = true;
}

if(empezar == true and obj_Personaje.invisible == false)
{
    if(obj_Personaje.x > x)
    {
        move = 1;
    }
    else if(obj_Personaje.x < x)
    {
        move = -1;
    }
    else
    {
        move = 0;
    }
}
else
{
    move = 0;
}
hsp = move * movespeed;


if (vsp < 10)
{
    vsp += grav;
}

if (place_meeting(x, y+vsp, obj_SueloN1) or place_meeting(x, y+vsp, obj_Pared))
{
    if(grounded != 0 && (place_meeting(x+hsp, y, obj_SueloN1) or place_meeting(x+hsp, y, obj_Pared)) )
    {
        hkp_count = 0;
        jumping = false
    }
    else
    {
        jumping = true;
    }
    
    grounded = 0;
    vsp = key_jump * -jumpspeed;
}

if(grounded == 0)
{
    hsp_jump_applied = 0;
}

if(move != 0 && grounded != 0)
{
    hkp_count++;
}
else if(move==0 && grounded != 0)
{
    hkp_count = 0;
}

if(jumping)
{
    if(hsp_jump_applied == 0)
    {
        hsp_jump_applied = sign(move)
    }
    
    if(hkp_count >= hkp_count_small)
    {
        hsp = 0;
    }
    else if(hkp_count >= hkp_count_small && hkp_count <= hkp_count_big)
    {
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }
    else
    {
        hsp = hsp_jump_applied * hsp_jump_constant_big;
    }
}

//colision horizontal
if(place_meeting(x+hsp, y, obj_Pared))
{
   while(!place_meeting(x+sign(hsp), y, obj_Pared))
   {
       x += sign(hsp);
   }
   hsp = 0;
}
x += hsp;

//colision vertical
if(place_meeting(x, y+vsp, obj_SueloN1) or place_meeting(x, y+vsp, obj_Pared) )
{
   while(!place_meeting(x, y+vsp, obj_SueloN1) or !place_meeting(x, y+sign(vsp), obj_Pared))
   {
       y += sign(vsp);
   }
   vsp = 0;
}
y += vsp;


if(vsp == 0)
{
    if(hsp == 0)
    {
        sprite_index = spr_enemigo2;
    }else{
        if ((sprite_index != spr_enemigo2_Left) and (sprite_index !=  sprite_index = spr_enemigo2_walk_Right))
        {
            image_index = 0;
        }
        if(hsp > 0)
        {
            sprite_index = spr_enemigo2_walk_Right;
        }
        else
        {
            sprite_index = spr_enemigo2_Left;
        }
    }   
       
}
else
{
    if (vsp < 0)
    {
        sprite_index = spr_enemigo2_jump;
    }
    else
    {
        sprite_index = spr_enemigo2_falling;
    }
}
